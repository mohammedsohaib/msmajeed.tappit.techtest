-- Adapted For Microsoft SQL Server As I Didn't Have A PostgreSQL Setup --
CREATE DATABASE TappitTechTest;
GO

USE TappitTechTest;
GO

DROP TABLE IF EXISTS Sports;
CREATE TABLE Sports(
	[SportId] INT,
	[Name] VARCHAR(50) NOT NULL,
	[IsEnabled] BIT NOT NULL,
 	CONSTRAINT PK_Sports PRIMARY KEY ([SportId]) 
);
INSERT INTO Sports ([SportId], [Name], [IsEnabled]) VALUES 
(1, N'American Football', 1),
(2, N'Baseball', 1),
(3, N'Basketball', 1);
GO

DROP TABLE IF EXISTS People;
CREATE TABLE People(
	PersonId INT,
	FirstName VARCHAR(50) NOT NULL,
	LastName VARCHAR(50) NOT NULL,
	IsAuthorised BIT NOT NULL,
	IsValid BIT NOT NULL,
	IsEnabled BIT NOT NULL,
 	CONSTRAINT PK_People PRIMARY KEY ([PersonId]) 
);
INSERT INTO People ([PersonId], [FirstName], [LastName], [IsAuthorised], [IsValid], [IsEnabled]) VALUES 
(1, N'Frank', N'Smith', 0, 1, 0),
(2, N'Bob', N'Mason', 0, 0, 0),
(3, N'David', N'Adams', 0, 1, 1),
(4, N'Eve', N'Jones', 0, 0, 0),
(5, N'Steven', N'Taylor', 0, 1, 1),
(6, N'Hannah', N'Butler', 0, 0, 0),
(7, N'John', N'Edwards', 0, 1, 0),
(8, N'Oliver', N'Woods', 0, 0, 0),
(9, N'Natan', N'Lee', 0, 1, 1),
(10, N'Thomas', N'Brown', 0, 1, 1),
(11, N'Otto', N'Campbell', 1, 1, 0);
GO

DROP TABLE IF EXISTS FavouriteSports;
CREATE TABLE FavouriteSports(
	PersonId INT NOT NULL,
	SportId INT NOT NULL,
 	CONSTRAINT PK_FavouriteSports PRIMARY KEY ([PersonId],[SportId]),
	CONSTRAINT FK_FavouriteSports_People FOREIGN KEY([PersonId]) REFERENCES People([PersonId]),
	CONSTRAINT FK_FavouriteSports_Sports FOREIGN KEY([SportId]) REFERENCES Sports([SportId])
);

INSERT INTO FavouriteSports ([PersonId], [SportId]) VALUES 
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(3, 2),
(4, 1),
(4, 2),
(4, 3),
(5, 2),
(6, 1),
(7, 2),
(7, 3),
(8, 2),
(9, 1),
(10, 1),
(10, 2),
(10, 3),
(11, 1);
GO