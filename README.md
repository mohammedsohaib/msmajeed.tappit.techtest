# Tappit Tech Test
## API Setup Guide
1. Run the Database\TappitTechTest.sql script or import the data-tier application file Database\TappitTechTest.bacpac in SSMS (SQL Server Management Studio)
2. Install .NET 6. This has been chosen since it has LTS (Long Term Suport) from Microsoft
3. API is located under Solution\MSMajeed.Tappit.TechTest.Api
4. Update the TappitTechTestDatabase connection string in appsettings.json under the MSMajeed.Tappit.TechTest.Api project
4. Right click the MSMajeed.Tappit.TechTest.Api project then click "Set as Statup Project"
5. Now hit start ensuring MSMajeed.Tappit.TechTest.Api is selected and not IIS
6. Navigate to https://localhost:7264/swagger/index.html in a web browser for the API
## UI Setup Guide
1. Open terminal on path Solution\MSMajeed.Tappit.TechTest.Ui and run the command npm install
2. Update API_URL in Solution\MSMajeed.Tappit.TechTest.Ui\src\environments\environment.ts
3. Run the command npm start
4. You should now be running the application successfully 