﻿using Dapper;
using Microsoft.Extensions.Logging;
using MSMajeed.Tappit.TechTest.Data.Models;
using MSMajeed.Tappit.TechTest.Data.Services.Interfaces;
using System.Data;

namespace MSMajeed.Tappit.TechTest.Data.Services
{
    public class PeopleService : IPeopleService
    {
        private readonly ILogger<PeopleService> _logger;
        private readonly TappitTechTestDataContext _dataContext;

        public PeopleService(ILogger<PeopleService> logger, TappitTechTestDataContext dataContext)
        {
            _logger = logger;
            _dataContext = dataContext;
        }

        public async Task UpdatePerson(int personId, PersonUpdate personUpdate)
        {
            using (var conn = _dataContext.CreateConnection())
            {
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("FirstName", personUpdate.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add("LastName", personUpdate.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
                dynamicParameters.Add("IsAuthorised", personUpdate.IsAuthorised, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                dynamicParameters.Add("IsValid", personUpdate.IsValid, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                dynamicParameters.Add("IsEnabled", personUpdate.IsEnabled, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                dynamicParameters.Add("PersonId", personId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                var query = @"UPDATE People 
                            SET FirstName = @FirstName 
	                            ,LastName = @LastName 
	                            ,IsAuthorised = @IsAuthorised 
	                            ,IsValid = @IsValid 
	                            ,IsEnabled = @IsEnabled
                            WHERE PersonId = @PersonId";

                await conn.ExecuteAsync(query, dynamicParameters);
                if (personUpdate.FavouriteSportsIds != null || personUpdate.FavouriteSportsIds.Count > 0)
                {
                    await UpdatePersonFavouriteSports(personId, personUpdate.FavouriteSportsIds);
                }
            }
        }

        private async Task UpdatePersonFavouriteSports(int personId, List<int> updatedFavouriteSportsIds)
        {
            using (var conn = _dataContext.CreateConnection())
            {
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("PersonId", personId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                var currentFavouriteSportsQuery = @"SELECT S.* FROM Sports S
                            JOIN FavouriteSports FS ON FS.SportId = S.SportId
                            WHERE FS.PersonId = @PersonId";
                var currentFavouriteSports = (await conn.QueryAsync<Sport>(currentFavouriteSportsQuery, dynamicParameters)).ToList();

                var favouriteSportsToDelete = new List<int>();
                var favouriteSportsToAdd = new List<int>();

                foreach (var sport in currentFavouriteSports)
                {
                    if (!updatedFavouriteSportsIds.Contains(sport.SportId))
                    {
                        favouriteSportsToDelete.Add(sport.SportId);
                    }
                }

                foreach (var sportId in updatedFavouriteSportsIds)
                {
                    if (!currentFavouriteSports.Select(s => s.SportId).Contains(sportId))
                    {
                        favouriteSportsToAdd.Add(sportId);
                    }
                }

                if (favouriteSportsToDelete != null && favouriteSportsToDelete.Count > 0)
                {
                    await DeleteFavouriteSports(personId, favouriteSportsToDelete);
                }

                if (favouriteSportsToAdd != null && favouriteSportsToAdd.Count > 0)
                {
                    await AddFavouriteSports(personId, favouriteSportsToAdd);
                }
            }
        }

        private async Task AddFavouriteSports(int personId, List<int> sportsIds)
        {
            using (var conn = _dataContext.CreateConnection())
            {
                foreach (var sportId in sportsIds)
                {
                    var dynamicParameters = new DynamicParameters();
                    dynamicParameters.Add("PersonId", personId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    dynamicParameters.Add("SportId", sportId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                    var query = @"INSERT INTO FavouriteSports (PersonId, SportId) Values (@PersonId, @SportId)";

                    await conn.ExecuteAsync(query, dynamicParameters);
                }
            }
        }

        private async Task DeleteFavouriteSports(int personId, List<int> sportsIds)
        {
            using (var conn = _dataContext.CreateConnection())
            {
                foreach (var sportId in sportsIds)
                {
                    var dynamicParameters = new DynamicParameters();
                    dynamicParameters.Add("PersonId", personId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                    dynamicParameters.Add("SportId", sportId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                    var query = @"DELETE FROM FavouriteSports WHERE PersonId = @PersonId AND SportId = @SportId";

                    await conn.ExecuteAsync(query, dynamicParameters);
                }
            }
        }

        public async Task<Person> GetPerson(int personId)
        {
            using (var conn = _dataContext.CreateConnection())
            {
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("PersonId", personId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                var query = @"SELECT 
	                            P.*
	                            ,S.* 
                            FROM People p 
                            LEFT JOIN FavouriteSports FS ON FS.PersonId = P.PersonId 
                            LEFT JOIN Sports S ON S.SportId = FS.SportId AND S.IsEnabled = 1
                            WHERE P.PersonId = @PersonId";

                Person existingPerson = null;
                await conn.QueryAsync<Person, Sport, Person>(query, (person, sport) =>
                {
                    if (existingPerson != null)
                    {
                        person = existingPerson;
                    }
                    else
                    {
                        person.FavouriteSports = new List<Sport>();
                        existingPerson = person;
                    }

                    person.FavouriteSports.Add(sport);
                    return person;
                }, dynamicParameters, splitOn: "SportId");

                return existingPerson;
            }
        }

        public async Task<IEnumerable<PersonWithPalindrome>> GetPeople()
        {
            using (var conn = _dataContext.CreateConnection())
            {
                var query = @"SELECT 
	                            P.*
	                            ,S.* 
                            FROM People p 
                            LEFT JOIN FavouriteSports FS ON FS.PersonId = P.PersonId 
                            LEFT JOIN Sports S ON S.SportId = FS.SportId AND S.IsEnabled = 1";

                var peopleMap = new Dictionary<int, PersonWithPalindrome>();
                await conn.QueryAsync<PersonWithPalindrome, Sport, PersonWithPalindrome>(query, (person, sport) =>
                {
                    if (peopleMap.TryGetValue(person.PersonId, out PersonWithPalindrome existingPerson))
                    {
                        person = existingPerson;
                    }
                    else
                    {
                        person.FavouriteSports = new List<Sport>();
                        person.IsPalindrome = IsPalindrome(person.FirstName);
                        peopleMap.Add(person.PersonId, person);
                    }

                    person.FavouriteSports.Add(sport);
                    return person;
                }, splitOn: "SportId");

                return peopleMap.Values;
            }
        }

        private bool IsPalindrome(string firstName)
        {
            var reversedFirstName = string.Empty;
            for (int i = firstName.Length - 1; i >= 0; i--)
            {
                reversedFirstName += firstName.ToLower()[i];
            }
            return firstName.ToLower() == reversedFirstName;
        }
    }
}
