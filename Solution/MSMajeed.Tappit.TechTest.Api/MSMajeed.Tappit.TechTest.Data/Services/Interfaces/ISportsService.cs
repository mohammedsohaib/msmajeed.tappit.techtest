﻿using MSMajeed.Tappit.TechTest.Data.Models;

namespace MSMajeed.Tappit.TechTest.Data.Services.Interfaces
{
    public interface ISportsService
    {
        Task<IEnumerable<SportWithFavouriteCount>> GetSports();
    }
}
