﻿using MSMajeed.Tappit.TechTest.Data.Models;

namespace MSMajeed.Tappit.TechTest.Data.Services.Interfaces
{
    public interface IPeopleService
    {
        Task<Person> GetPerson(int personId);
        Task UpdatePerson(int personId, PersonUpdate personUpdate);
        Task<IEnumerable<PersonWithPalindrome>> GetPeople();
    }
}
