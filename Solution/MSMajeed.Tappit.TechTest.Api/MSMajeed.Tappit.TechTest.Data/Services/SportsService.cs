﻿using Dapper;
using Microsoft.Extensions.Logging;
using MSMajeed.Tappit.TechTest.Data.Models;
using MSMajeed.Tappit.TechTest.Data.Services.Interfaces;

namespace MSMajeed.Tappit.TechTest.Data.Services
{
    public class SportsService: ISportsService
    {
        private readonly ILogger<SportsService> _logger;
        private readonly TappitTechTestDataContext _dataContext;

        public SportsService(ILogger<SportsService> logger, TappitTechTestDataContext dataContext)
        {
            _logger = logger;
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<SportWithFavouriteCount>> GetSports()
        {
            using (var conn = _dataContext.CreateConnection())
            {
                var query = @"SELECT 
	                            S.*
	                            ,(SELECT COUNT(*) FROM FavouriteSports FS WHERE FS.SportId = S.SportId) AS FavouriteCount 
                            FROM Sports S
                            WHERE S.IsEnabled = 1";

                var sports = await conn.QueryAsync<SportWithFavouriteCount>(query);

                return sports;
            }
        }
    }
}
