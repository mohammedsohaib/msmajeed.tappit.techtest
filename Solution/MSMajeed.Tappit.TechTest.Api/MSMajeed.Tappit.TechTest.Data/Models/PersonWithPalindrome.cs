﻿namespace MSMajeed.Tappit.TechTest.Data.Models
{
    public class PersonWithPalindrome : Person
    {
        public bool IsPalindrome { get; set; }
    }
}
