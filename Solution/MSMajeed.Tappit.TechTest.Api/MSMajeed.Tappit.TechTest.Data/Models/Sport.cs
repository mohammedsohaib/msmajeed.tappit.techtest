﻿namespace MSMajeed.Tappit.TechTest.Data.Models
{
    public class Sport
    {
        public int SportId { get; set; }
        public string Name { get; set; }
    }
}
