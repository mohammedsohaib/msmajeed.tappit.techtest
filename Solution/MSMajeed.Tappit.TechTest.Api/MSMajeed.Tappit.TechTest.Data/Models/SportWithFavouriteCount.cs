﻿namespace MSMajeed.Tappit.TechTest.Data.Models
{
    public class SportWithFavouriteCount : Sport
    {
        public int FavouriteCount { get; set; }
    }
}
