﻿namespace MSMajeed.Tappit.TechTest.Data.Models
{
    public class PersonUpdate
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsAuthorised { get; set; }
        public bool IsValid { get; set; }
        public bool IsEnabled { get; set; }
        public List<int> FavouriteSportsIds { get; set; }
    }
}
