﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Data;
using System.Data.SqlClient;

namespace MSMajeed.Tappit.TechTest.Data
{
    public class TappitTechTestDataContext
    {
        private readonly ILogger<TappitTechTestDataContext> _logger;
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public TappitTechTestDataContext(ILogger<TappitTechTestDataContext> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("TappitTechTestDatabase");
        }

        public IDbConnection CreateConnection() => new SqlConnection(_connectionString);
    }
}
