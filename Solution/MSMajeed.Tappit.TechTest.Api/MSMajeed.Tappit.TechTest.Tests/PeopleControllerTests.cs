using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using MSMajeed.Tappit.TechTest.Controllers;
using MSMajeed.Tappit.TechTest.Data.Models;
using MSMajeed.Tappit.TechTest.Data.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Xunit;

namespace MSMajeed.Tappit.TechTest.Tests
{
    public class PeopleControllerTests : IDisposable
    {
        private IEnumerable<Sport>? mockSports;
        private IEnumerable<Person>? mockPeople;
        private IEnumerable<PersonWithPalindrome>? mockPeopleWithPalindrome;
        private PeopleController? peopleController;

        public PeopleControllerTests()
        {
            mockSports = new List<Sport>() {
                new Sport() { SportId = 1,  Name = "Cricket" },
                new Sport() { SportId = 2,  Name = "Football" },
                new Sport() { SportId = 3,  Name = "Table Tennis" }
            };

            mockPeople = new List<Person>()
            {
                new Person() { PersonId = 1, FirstName = "Test", LastName = "Person" },
                new Person() { PersonId = 2, FirstName = "Bob", LastName = "Builder" },
                new Person() { PersonId = 3, FirstName = "Sports", LastName = "Man",
                    FavouriteSports = new List<Sport>()
                },
            };
            mockPeople.Single(p => p.PersonId == 3).FavouriteSports.Add(mockSports.Single(s => s.SportId == 3));

            mockPeopleWithPalindrome = new List<PersonWithPalindrome>()
            {
                new PersonWithPalindrome() { PersonId = 1, FirstName = "Test", LastName = "Person" },
                new PersonWithPalindrome() { PersonId = 2, FirstName = "Bob", LastName = "Builder", IsPalindrome = true },
                new PersonWithPalindrome() { PersonId = 3, FirstName = "Sports", LastName = "Man", 
                    FavouriteSports = new List<Sport>()
                },
            };
            mockPeopleWithPalindrome.Single(p => p.PersonId == 3).FavouriteSports.Add(mockSports.Single(s => s.SportId == 3));

            var mockLogger = new Mock<ILogger<PeopleController>>();
            var mockPeopleService = new Mock<IPeopleService>();
            mockPeopleService.Setup(m => m.GetPeople()).Returns(Task.FromResult(mockPeopleWithPalindrome));
            mockPeopleService.Setup(m => m.GetPerson(It.IsAny<int>())).Returns<int>((personId) => { 
                return Task.FromResult(mockPeople.SingleOrDefault(p => p.PersonId == personId)); 
            });
            mockPeopleService.Setup(m => m.UpdatePerson(It.IsAny<int>(), It.IsAny<PersonUpdate>()))
                .Returns<int, PersonUpdate>((personId, personUpdate) => {
                    var mockPerson = mockPeople.SingleOrDefault(p => p.PersonId == personId);
                    if (mockPerson != null)
                    {
                        mockPerson.FirstName = personUpdate.FirstName;
                        mockPerson.LastName = personUpdate.LastName;
                        mockPerson.IsEnabled = personUpdate.IsEnabled;
                        mockPerson.IsValid = personUpdate.IsValid;
                        mockPerson.IsAuthorised = personUpdate.IsAuthorised;
                        mockPerson.FavouriteSports = mockSports.Where(s => personUpdate.FavouriteSportsIds.Contains(s.SportId)).ToList();
                    }
                    return Task.CompletedTask;
                });

            peopleController = new PeopleController(mockLogger.Object, mockPeopleService.Object);
        }

        public void Dispose()
        {
            mockSports = null;
            mockPeople = null;
            mockPeopleWithPalindrome = null;
            peopleController = null;
        }

        [Fact]
        public async Task GetPeopleTest()
        {
            var actionResult = await peopleController.GetPeople();
            Assert.NotNull(actionResult);

            var okObjectResult = actionResult as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.True(okObjectResult.StatusCode == StatusCodes.Status200OK);

            var people = okObjectResult.Value as List<PersonWithPalindrome>;
            Assert.NotNull(people);
            Assert.True(people.Count == mockPeopleWithPalindrome.Count());
        }

        [Fact]
        public async Task GetPersonTest()
        {
            var actionResult = await peopleController.GetPerson(1);
            Assert.NotNull(actionResult);

            var okObjectResult = actionResult as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.True(okObjectResult.StatusCode == StatusCodes.Status200OK);

            var person = okObjectResult.Value as Person;
            Assert.NotNull(person);
            Assert.True(person.PersonId == 1);
        }

        [Fact]
        public async Task UpdatePersonTest()
        {
            var person = mockPeople.Single(p => p.PersonId == 1);
            var personUpdate = new PersonUpdate
            {
                FirstName = "Updated",
                LastName = "Person",
                IsEnabled = person.IsEnabled,
                IsValid = person.IsValid,
                IsAuthorised = person.IsAuthorised,
                FavouriteSportsIds = mockSports.Select(s => s.SportId).ToList()
            };

            var updateActionResult = await peopleController.UpdatePerson(person.PersonId, personUpdate);
            Assert.NotNull(updateActionResult);

            var updateOkResult = updateActionResult as OkResult;
            Assert.NotNull(updateOkResult);
            Assert.True(updateOkResult.StatusCode == StatusCodes.Status200OK);

            var getActionResult = await peopleController.GetPerson(person.PersonId);
            Assert.NotNull(getActionResult);

            var getOkObjectResult = getActionResult as OkObjectResult;
            Assert.NotNull(getOkObjectResult);
            Assert.True(getOkObjectResult.StatusCode == StatusCodes.Status200OK);

            var updatedPerson = getOkObjectResult.Value as Person;
            Assert.NotNull(updatedPerson);
            Assert.True(updatedPerson.PersonId == person.PersonId);
            Assert.True(updatedPerson.FirstName == personUpdate.FirstName);
            Assert.True(updatedPerson.LastName == personUpdate.LastName);
            Assert.True(updatedPerson.FavouriteSports.Count() == personUpdate.FavouriteSportsIds.Count());
        }
    }
}