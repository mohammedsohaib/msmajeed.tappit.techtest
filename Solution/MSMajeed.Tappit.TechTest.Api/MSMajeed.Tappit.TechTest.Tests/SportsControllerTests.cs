using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using MSMajeed.Tappit.TechTest.Controllers;
using MSMajeed.Tappit.TechTest.Data.Models;
using MSMajeed.Tappit.TechTest.Data.Services.Interfaces;
using Xunit;

namespace MSMajeed.Tappit.TechTest.Tests
{
    public class SportsControllerTests: IDisposable
    {
        private IEnumerable<SportWithFavouriteCount>? mockSportsWithFavouriteCount;
        private SportsController? sportsController;

        public SportsControllerTests()
        {
            mockSportsWithFavouriteCount = new List<SportWithFavouriteCount>() {
                new SportWithFavouriteCount() { SportId = 1,  Name = "Cricket", FavouriteCount = 1 },
                new SportWithFavouriteCount() { SportId = 2,  Name = "Football", FavouriteCount = 50},
                new SportWithFavouriteCount() { SportId = 3,  Name = "Table Tennis", FavouriteCount = 6 }
            };

            var mockLogger = new Mock<ILogger<SportsController>>();
            var mockSportService = new Mock<ISportsService>();
            mockSportService.Setup(m => m.GetSports()).Returns(Task.FromResult(mockSportsWithFavouriteCount));

            sportsController = new SportsController(mockLogger.Object, mockSportService.Object);
        }

        public void Dispose()
        {
            mockSportsWithFavouriteCount = null;
            sportsController = null;
        }

        [Fact]
        public async Task GetAvailableSportsTest()
        {
            var actionResult = await sportsController.GetAvailableSports();
            Assert.NotNull(actionResult);

            var okObjectResult = actionResult as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.True(okObjectResult.StatusCode == StatusCodes.Status200OK);

            var sports = okObjectResult.Value as List<SportWithFavouriteCount>;
            Assert.NotNull(sports);
            Assert.True(sports.Count == mockSportsWithFavouriteCount.Count());
        }
    }
}