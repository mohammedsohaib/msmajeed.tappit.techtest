using Microsoft.AspNetCore.Mvc;
using MSMajeed.Tappit.TechTest.Data.Models;
using MSMajeed.Tappit.TechTest.Data.Services.Interfaces;

namespace MSMajeed.Tappit.TechTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PeopleController : ControllerBase
    {
        private readonly ILogger<PeopleController> _logger;
        private readonly IPeopleService _peopleService;

        public PeopleController(ILogger<PeopleController> logger, IPeopleService peopleService)
        {
            _logger = logger;
            _peopleService = peopleService;
        }

        /// <summary>
        /// Updates the person.
        /// </summary>
        /// <param name="personId">The person identifier.</param>
        /// <param name="personUpdate">The person update.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{personId}/person")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdatePerson(int personId, PersonUpdate personUpdate)
        {
            try
            {
                if (personId < 1)
                {
                    return BadRequest($"Invalid personId:{personId}. Provide valid personId");
                }

                var person = await _peopleService.GetPerson(personId);
                if (person == null)
                {
                    return NotFound($"Person not found with personId:{personId}");
                }

                await _peopleService.UpdatePerson(personId, personUpdate);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Gets the person.
        /// </summary>
        /// <param name="personId">The person identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{personId}/person")]
        [ProducesResponseType(typeof(Person), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetPerson(int personId)
        {
            try
            {
                if (personId < 1)
                {
                    return BadRequest($"Invalid personId:{personId}. Provide valid personId");
                }

                var person = await _peopleService.GetPerson(personId);
                if (person == null)
                {
                    return NotFound($"Person not found with personId:{personId}");
                }

                return Ok(person);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Gets the people.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PersonWithPalindrome>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPeople()
        {
            try
            {
                var people = await _peopleService.GetPeople();
                return Ok(people);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}