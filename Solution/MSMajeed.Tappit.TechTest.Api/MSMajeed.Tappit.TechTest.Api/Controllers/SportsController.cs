using Microsoft.AspNetCore.Mvc;
using MSMajeed.Tappit.TechTest.Data.Models;
using MSMajeed.Tappit.TechTest.Data.Services.Interfaces;

namespace MSMajeed.Tappit.TechTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SportsController : ControllerBase
    {
        private readonly ILogger<SportsController> _logger;
        private readonly ISportsService _sportsService;

        public SportsController(ILogger<SportsController> logger, ISportsService sportsService)
        {
            _logger = logger;
            _sportsService = sportsService;
        }

        /// <summary>
        /// Gets the available sports.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<SportWithFavouriteCount>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAvailableSports()
        {
            try
            {
                var sports = await _sportsService.GetSports();
                return Ok(sports);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}