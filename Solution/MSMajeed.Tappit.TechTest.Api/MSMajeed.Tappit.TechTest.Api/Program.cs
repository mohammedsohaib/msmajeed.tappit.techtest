using MSMajeed.Tappit.TechTest.Data;
using MSMajeed.Tappit.TechTest.Data.Services;
using MSMajeed.Tappit.TechTest.Data.Services.Interfaces;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add dependencies
builder.Services.AddSingleton<TappitTechTestDataContext>();
builder.Services.AddSingleton<ISportsService, SportsService>();
builder.Services.AddSingleton<IPeopleService, PeopleService>();

// Lowercase routing
builder.Services.AddRouting(options => options.LowercaseUrls = true);

// Add services to the container
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(o => o.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml")));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(policy => policy.AllowAnyHeader()
                            .AllowAnyMethod()
                            .SetIsOriginAllowed(origin => true)
                            .AllowCredentials());

app.UseAuthorization();

app.MapControllers();

app.Run();
