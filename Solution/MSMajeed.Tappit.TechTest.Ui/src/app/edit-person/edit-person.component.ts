import { Component, OnInit } from '@angular/core';
import { PeopleService } from 'src/services/people.service';
import { SportsService } from 'src/services/sports.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from 'src/models/person';
import { Sport } from 'src/models/sport.model';
import { PersonUpdate } from 'src/models/person-update.model';

@Component({
  selector: 'app-edit-person',
  templateUrl: './edit-person.component.html',
  styleUrls: ['./edit-person.component.css']
})
export class EditPersonComponent implements OnInit {
  public person!: Person;
  public availableSports: Sport[] = [];

  constructor(private route: ActivatedRoute, private router: Router, private readonly peopleService: PeopleService,
    private readonly sportsService: SportsService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.sportsService.GetAvailableSports()
        .subscribe((data) => data.map(d => this.availableSports.push({ sportId: d.sportId, name: d.name, isFavourited: false })));
      this.peopleService.GetPerson(params['personId'])
        .subscribe(data => {
          this.person = data;
          if (this.availableSports != null) {
            this.availableSports.forEach(s => s.isFavourited = this.person.favouriteSports.some(fs => fs.sportId == s.sportId));
          }
        });
    });
  }

  updatePerson() {
    var personUpdate: PersonUpdate = {
      firstName: this.person.firstName,
      lastName: this.person.lastName,
      isEnabled: this.person.isEnabled,
      isValid: this.person.isValid,
      isAuthorised: this.person.isAuthorised,
      favouriteSportsIds: this.availableSports.filter(s => s.isFavourited).map(s => s.sportId)
    }

    this.peopleService.UpdatePerson(this.person.personId, personUpdate).subscribe(() => this.router.navigate(['/']));
  }
}