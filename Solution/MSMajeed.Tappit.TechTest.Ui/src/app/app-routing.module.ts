import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PeopleDashboardComponent } from 'src/app/people-dashboard/people-dashboard.component';
import { EditPersonComponent } from 'src/app/edit-person/edit-person.component';

const routes: Routes = [
  { path: '', component: PeopleDashboardComponent },
  { path: 'person/:personId', component: EditPersonComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
