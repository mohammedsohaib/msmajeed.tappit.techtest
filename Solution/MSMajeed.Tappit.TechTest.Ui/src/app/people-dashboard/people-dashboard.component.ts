import { Component, OnInit } from '@angular/core';
import { PeopleService } from 'src/services/people.service'
import { PersonWithPalindrome } from 'src/models/person-with-palindrome.model';
import { Sport } from 'src/models/sport.model';
import { Person } from 'src/models/person';
import { Router } from '@angular/router';

@Component({
  selector: 'app-people-dashboard',
  templateUrl: './people-dashboard.component.html',
  styleUrls: ['./people-dashboard.component.css']
})
export class PeopleDashboardComponent implements OnInit {
  public loading: boolean = true;
  public people!: PersonWithPalindrome[];

  constructor(private readonly router: Router, private readonly peopleService: PeopleService) { }

  ngOnInit() {
    this.peopleService.GetPeople().subscribe(data => {
      this.people = data;
      this.loading = false;
    });
  }

  getFavouriteSportsColumn(person: Person) {
    if (person.favouriteSports == null || person.favouriteSports.length < 1) {
      return '';
    } else {
      return person.favouriteSports.map((s: Sport) => s?.name).join(', ');
    }
  }

  goToPerson(personId: number) {
    this.router.navigate(['person', personId]);
  }
}