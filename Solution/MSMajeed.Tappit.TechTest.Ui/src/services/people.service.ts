import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { PersonWithPalindrome } from 'src/models/person-with-palindrome.model';
import { Observable } from 'rxjs';
import { Person } from 'src/models/person';
import { PersonUpdate } from 'src/models/person-update.model';

@Injectable({
    providedIn: 'root',
})
export class PeopleService {
    private API_URL = environment.API_URL;

    constructor(private http: HttpClient) { }

    UpdatePerson(personId: number, personUpdate: PersonUpdate): Observable<any> {
        return this.http.put(`${this.API_URL}/people/${personId}/person`, personUpdate);
    };

    GetPerson(personId: number): Observable<Person> {
        return this.http.get<Person>(`${this.API_URL}/people/${personId}/person`);
     }

    GetPeople(): Observable<PersonWithPalindrome[]> {
       return this.http.get<PersonWithPalindrome[]>(`${this.API_URL}/people`);
    }
}