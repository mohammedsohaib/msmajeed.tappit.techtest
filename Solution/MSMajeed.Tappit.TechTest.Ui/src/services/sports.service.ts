import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SportWithFavouriteCount } from 'src/models/sport-with-favourite-count.model';

@Injectable({
    providedIn: 'root',
})
export class SportsService {
    private API_URL = environment.API_URL;

    constructor(private http: HttpClient) { }

    GetAvailableSports(): Observable<SportWithFavouriteCount[]> {
        return this.http.get<SportWithFavouriteCount[]>(`${this.API_URL}/sports`);
    }
}