import { Sport } from "src/models/sport.model";

export class Person {
    public personId!: number;
    public firstName!: string;
    public lastName!: string;
    public isAuthorised!: boolean;
    public isValid!: boolean;
    public isEnabled!: boolean;
    public favouriteSports!: Sport[];
}