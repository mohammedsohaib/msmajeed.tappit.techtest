export class PersonUpdate {
    public firstName!: string;
    public lastName!: string;
    public isAuthorised!: boolean;
    public isValid!: boolean;
    public isEnabled!: boolean;
    public favouriteSportsIds!: number[];
}