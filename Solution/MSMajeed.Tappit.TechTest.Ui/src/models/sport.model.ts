export class Sport {
    public sportId!: number;
    public name!: string;
    public isFavourited!: boolean;
}