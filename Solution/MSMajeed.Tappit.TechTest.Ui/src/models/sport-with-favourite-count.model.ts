export class SportWithFavouriteCount {
    public sportId!: number;
    public name!: string;
    public favouriteCount!: number;
}